******************************************************************
In this repository I will store ARM templates and PowerShell codes
******************************************************************

* `Deploy two Web App with ARM template <https://github.com/jamalshahverdiev/arm-powershell-codes/tree/master/ARM-Template-Examples>`_
* `PowerShell Script Examples <https://github.com/jamalshahverdiev/arm-powershell-codes/tree/master/PowerShell-Codes>`_
* `Azure PowerShell documents <https://github.com/jamalshahverdiev/arm-powershell-codes/tree/master/Azure-PowerShell-Docs>`_


